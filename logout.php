<?php
   session_start();
   
   //destrueix la sessió. AIxò borra totes les variables de session. (incloent login_user que es la que feiem servir com a Auth)
   if(session_destroy()) {
      header("Location: login.php");
   }
?>