<?php
// Recordeu que el session_start ha d'estar sempre a tot arreu
session_start();

// Mira si hi ha sessió de login_user. Es a dir, si existeix la variable global "login_user"
if(isset($_SESSION['login_user'])) {
  echo '<p>Logged in as</p>';
  ///l'acces a la variable es directe i es pot concatenar
  echo '<p>' . $_SESSION['login_user'] . '</p>';
  echo '<p><a href="/?logout">Log Out</a></p>';
  //die();
}

// Mira si no hi ha sessió. Vindria a ser un else de l'if anterior
if(!isset($_SESSION['login_user'])) {
  echo '<p>Not logged in</p>';
  echo '<p><a href=login.php>Log In</a></p>';
}






