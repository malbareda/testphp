<?php
//aquest snippet el podem posar a totes les pagines que han d'estar restringides per Auth. D'aquesta forma ens estalviem llegir el codi
   include('config.php');
   session_start();
   
   //si hi ha sessió
   if(isset($_SESSION['login_user'])){
       //posem el nom d'usuari en una variable per a poder-la fer servir
       $login_session = $_SESSION['login_user'];
   }

//si no hi ha sessió
   if(!isset($_SESSION['login_user'])){
       //reenviem al login i no deixem continuar.
      header("location:login.php");
      die();
   }
?>